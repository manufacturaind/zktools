SSH_PATH=manufactura:~/apps/memoreads/myapp/

install:
	python3 -m venv .env
	. `pwd`/.env/bin/activate; pip install -r requirements.txt

serve: pull-content
	. `pwd`/.env/bin/activate; python server.py

pull-content:
	mkdir -p content
	rsync -aruz ~/nextcloud/Manufactura/memoreads/* content/ --exclude="media/"

deploy: pull-content 
	rsync --compress --checksum --progress --recursive --update . $(SSH_PATH) --exclude="__pycache__" --exclude=".*" 
	rsync --compress --checksum --progress --recursive --update ~/nextcloud/Manufactura/memoreads/media/ manufactura:~/apps/memoreads-media/ --exclude=".*" 
	ssh manufactura -C "touch ~/apps/memoreads/reload.txt"
dry-deploy: pull-content
	rsync --compress --checksum --progress --recursive --update . $(SSH_PATH) --exclude="__pycache__" --exclude=".*" --dry-run 
	rsync --compress --checksum --progress --recursive --update ~/nextcloud/Manufactura/memoreads/media/ manufactura:~/apps/memoreads-media/ --exclude=".*" --dry-run
