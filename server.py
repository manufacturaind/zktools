import bottle
from bottle import get, run, request, static_file, route, response, hook, auth_basic
try:
    from zklint import Note, get_all_notes
except ModuleNotFoundError:
    from myapp.zklint import Note, get_all_notes
import os
import click
import json
import magic
import markdown
import hashlib
import re
from pathlib import Path

import socket
hostname = socket.gethostname()

if 'opalstack' in hostname:
    BASE_PATH = os.path.join(os.getcwd(), "myapp/content/")
    FRONTEND_PATH = "myapp/frontend/"
else:
    BASE_PATH = os.path.join(os.getcwd(), "content/")
    FRONTEND_PATH = "frontend/"

md = markdown.Markdown(extensions=['meta', 'wikilinks', 'fenced_code', 'tables', 'nl2br'])

app = application = bottle.Bottle()

names = [
    ['Hans', 'hans', 'Hans Lammerant',],
    ['Sarrita', 'sarrita',],
    ['wendy', 'Wendy', 'Wendy Van Wynsberghe',],
    ['francis', 'Francis', 'Francis Hunger',],
    ['rares', 'Rares Craiut', 'Rares', 'Rareș',],
    ['Donatella', 'donatella', 'donatellaconstant', 'Donatella Portoghese', 'DonaP',],
    ['Kate', 'kate rich', 'kate',],
    ['FS', 'femke', 'Femke', 'Femke Snelting','F_S',],
    # ['H'],
    ['Indieterminacy', 'indieterminacy', 'Ann Mertens', 'Ann',],
    ['Peter Hermans', 'peter_h/h',],
    ['peter', 'pewter', 'Peter', 'Peter Westenberg', 'PeterW',],
    ['Loup Cellard', 'Loup',],
    ['Simon', 'Simon Browne', 'Simoon', 'simoon', 'Simoooon',],
    ['lucia', 'Lucia Palladino', 'Lucia',],
    [r'O.J.A.I.', 'o.j.a.i.', 'OJAI',],
    ['chris', 'Nord', 'Chris Dreier', 'Chris',],
    ['Sud (Brussels)', 'Brussels', 'Sud', 'Gary Farrelly',],
    ['cate', 'xcate', 'Xcate',],
    ['dia', 'Dia', 'Dia Hamed',],
    ['ana',  'Ana',],
    ['Wheeler Richard', 'Richard Wheeler', 'WheelerRichard', 'rruuww', 'Richard',],
    ['&', '(she)', '(they)'],
    [ 'isi', 'isi_she', 'Isabel Paehr', 'isi (she)', 'Isi(She)',],
    ['Ren (they)', 'ren (they)', 'Ren(They)', 'ren', 'Ren', 'lo-ren_they-them', 'Loren Britton',],
    ['luluganeta', 'Ricardo Lafuente', 'Ricardo',],
    ['aiscarvalho', 'Ana Isabel Carvalho',],
    ['Manuf', 'Manufactura Independente',],
    ['Tamar et Yaën', 'Tamar et Yaen','yaen and tamar',],
    ['tamar',],
    ['Elodie', 'elodie', 'elo',],
    ['eric', 'Eric'],
    ['gloria gonzález fuster', 'gloria',],
    ['nA',],
    ['brendan', 'Brendan',],
    ['Jolien', 'JOLIEN', 'jolien', '(DPO, BE DPA)'],
    ['MELT',],
]



def stringhash12(s):
    # create a md5 hash from a string and return its first 12 characters
    return hashlib.md5(s.encode('utf-8')).hexdigest()[:12]




# set up CORS
# https://stackoverflow.com/a/41605919
@app.route('/<:re:.*>', method='OPTIONS')
def enable_cors_generic_route():
    add_cors_headers()


@app.hook('after_request')
def enable_cors_after_request_hook():
    add_cors_headers()


def add_cors_headers():
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = \
        'GET, POST, PUT, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = \
        'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'


# dumb auth
def check_auth(user, pw):
    if user == 'bureau' and pw == 'cracksy':
        return True
    return False


# Routes #############################

@app.route('/')
@auth_basic(check_auth)
def static_root():
    return static_file("index.html", root=FRONTEND_PATH)


@app.get('/api/notes/')
@auth_basic(check_auth)
def browse():
    output = []
    for day in (1,2,3,4,5,6):
        all_notes = get_all_notes(os.path.join(BASE_PATH, 'day-' + str(day)))
        for note in all_notes:
            contents = md.convert(open(note.filepath, 'r').read())
            title = md.Meta.get('title', note.name)
            # print(md.Meta)
            info = note.metadata
            info['day'] = day
            info['name'] = note.filepath.name.split('.')[0]
            info['title'] = title[0]
            info['date'] = md.Meta.get('date', [''])[0]
            # info['author'] = md.Meta.get('author', [''])[0]
            # info['sentby'] = md.Meta.get('sentby', [''])[0]
            info['author'] = stringhash12(md.Meta.get('author', [''])[0])
            info['sentby'] = stringhash12(md.Meta.get('sentby', [''])[0])
            info['link'] = md.Meta.get('link', [''])[0]
            info['size'] = min(10, len(contents)/100)
            info['acronym'] = ''.join([n[0] for n in title[0].split(' ')])
            if md.Meta.get('tags'):
                info['tags'] = [tag.strip() for tag in md.Meta.get('tags')[0].split(',')]
            else:
                info['tags'] = []
            output.append(info)
    return {'notes': output, 'days': (1,2,3,4,5,6)}


@app.get('/api/note/')
@auth_basic(check_auth)
def open_file():
    name = request.query.name
    filepath = ''
    for daynumber in (1,2,3,4,5,6):
        p = os.path.join(BASE_PATH, 'day-' + str(daynumber) + '/', name + ".md")
        if os.path.exists(p):
            filepath = p
            break
    if not filepath:
        return

    contents = md.convert(open(filepath, 'r').read())
    # obfuscate names
    for namegroup in names:
        namegroup.sort(key=len)
        namegroup.reverse()
        hash = stringhash12(namegroup[0]).upper()
        for name in namegroup:
            contents = re.sub(r'\b' + name + r'\b',
                              '<span class="hash">' + hash[:6] + '</span><span class="hash">' + hash[6:12] + '</span>',
                              contents)
    # obfuscate pad links
    contents = re.sub('https://pad.constantvzw.org/[\w\d:#@%/;$()~_?\+-=\\\.&>]*', '[REDACTED PAD URL]', contents)
    # obfuscate audio links
    contents = re.sub('https://bbb.constantvzw.org/podcast/[\w\d:#@%/;$()~_?\+-=\\\.&>]*', '[REDACTED AUDIO URL]', contents)

    title = md.Meta.get('title')[0]

    context = {'contents': contents,
               'name': name,
               'title': title,
               'date': md.Meta.get('date', [''])[0],
               # 'author': md.Meta.get('author', [''])[0],
               'sentby': md.Meta.get('sentby', [''])[0],
               'author': stringhash12(md.Meta.get('author', [''])[0]),
               # 'sentby': stringhash12(md.Meta.get('sentby', [''])[0]),
               'link': md.Meta.get('link', [''])[0],
               'size': min(10, len(contents)/100),
               'acronym': ''.join([n[0] for n in title.split(' ')]),
               'chat': False
            }
    if md.Meta.get('tags'):
        context['tags'] = [tag.strip() for tag in md.Meta.get('tags')[0].split(',')]
    else:
        context['tags'] = []

    # special formatting for public chats
    if 'public chat' in title.lower():
        context['chat'] = True

    return context


@app.route('/thumbnails/<path:re:.*>')
def static_thumbs(path):
    filename = THUMBNAIL_DIR + path
    return static_file(filename, root='.')


@app.route('/static/<filepath:path>')
def static_set(filepath):
    return static_file(filepath, root=FRONTEND_PATH)


@app.route('/raw/<filepath:path>')
def static_rawfile(filepath):
    return static_file(filepath, root=BASE_PATH)


# Init ###############################################

@click.command()
def serve():
    if 'opalstack' in hostname:
        run(app=app, host='0.0.0.0', port=27216)
    else:
        run(app=app, host='0.0.0.0', port=8000, reloader=True)

if __name__ == "__main__":
    serve()
